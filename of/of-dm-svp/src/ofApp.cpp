#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup()
{
    ofSetFrameRate(60);
    parameters.setName("parameters");
    params_system.setName("system");
    params_player.setName("player");

    gui.setup("svplay");
    
    //some path, may be absolute or relative to bin/data
    string path = "";
	cout << "test" << endl;
    ofDirectory dir(path);
	    //go through and print out all the paths
    dir.listDir();
   for(int i = 0; i < dir.size(); i++)
   {
        cout<<(dir.getPath(i))<<endl;
   }
    //only show png files
    dir.allowExt("mov");
    //dir.sort();
    //populate the directory object
    dir.listDir();
    dir.sort();


    
    
    params_system.add(enable_all.set("enable_all",0));
    enable_all.addListener(this, &ofApp::enable_all_changed);
    params_system.add(calibration_all.set("calibration_all",0));
    calibration_all.addListener(this, &ofApp::calibration_all_changed);
    gui.add(&params_system);
    params_system.add(matrix_column.set("matrix_col",12,1,16));
    params_system.add(matrix_w.set("matrix_w",85,50,320));
    params_system.add(matrix_offset.set("offset",3,0,50));
    params_system.add(fbo_w.set("fbo_width",1400,640,1920));
    params_system.add(fbo_h.set("fbo_height",1050,360,1080));
    
    for(int i=0; i < SVP_SIZE; i++)
    {
        svplayer[i].setup(i, 320, 320);
        svplayer[i].set_movie_dir(dir);
        params_player.add(svplayer[i].params);
        svplayer[i].load_index(i);
    }
    gui.add(&params_player);
    params_player.minimizeAll();
    gui.minimizeAll();
    
    
    sync.setup((ofParameterGroup&)gui.getParameter(),6667,"localhost",6666);
    fbo.allocate(fbo_w, fbo_h);
#ifdef SYPHON
    syphon_server.setName("dm-main");
#endif    
#ifdef SPOUT
	spout_server.init("dm-main");
#endif
	gui.loadFromFile("settings.xml");
    enable_all=1;
}

//--------------------------------------------------------------
void ofApp::update()
{
    sync.update();
    for(int i=0; i < SVP_SIZE; i++)
    {
       svplayer[i].update();
    }
}

//--------------------------------------------------------------
void ofApp::draw()
{
    fbo.begin();
    ofBackground(0, 0, 0);
    //svplayer[0].draw(0,0,200,200);
    for(int i=0; i < SVP_SIZE; i++)
    {
        int x = matrix_w*(i%matrix_column)+(matrix_offset)*(i%matrix_column);
        int y = matrix_w*(i/matrix_column)+(matrix_offset)*(i/matrix_column);
       svplayer[i].draw(x,y,matrix_w,matrix_w);
    }
    fbo.end();
    
    fbo.draw(0, 0);
#ifdef SYPHON
    syphon_server.publishTexture(&fbo.getTexture());
#endif    
#ifdef SPOUT
	spout_server.send(fbo.getTexture());
#endif
    gui.draw();
    ofDrawBitmapString(ofGetFrameRate(), 10,10);
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){

}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}


//--------------------------------------------------------------

void ofApp::num_player_changed(int &num)
{



}
//--------------------------------------------------------------
void ofApp::enable_all_changed(bool &b)
{
    for(int i=0; i < SVP_SIZE; i++)
    {
        svplayer[i].enable=b;
    }
}


//--------------------------------------------------------------

void ofApp::calibration_all_changed(bool &b)
{
    for(int i=0; i < SVP_SIZE; i++)
    {
        svplayer[i].calibration=b;
    }
}



void ofApp::set_mov_folder()
{
    
}
