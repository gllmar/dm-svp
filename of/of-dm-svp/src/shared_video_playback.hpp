//
//  shared_video_playback.hpp
//  vvideo_syphon
//
//  Created by gllm on 2021-11-06.
//

#ifndef shared_video_playback_hpp
#define shared_video_playback_hpp

#include <stdio.h>
#include "ofMain.h"
#include "ofxGui.h"
#include "ofxHapPlayer.h"

//#define SYPHON 1
#ifdef SYPHON 
	#include "ofxSyphon.h"
#endif
#define SPOUT 1
#ifdef SPOUT
#include "ofxSpout.h"
#endif

class svplay
{
    public:
    svplay(){};
    

    ofParameter<bool> is_loading=0;
    ofParameter<bool> is_hap=0;
    ofParameter<bool> enable=1;
    ofParameter<bool> calibration=0;

 
    ofxHapPlayer player;
    ofFbo fbo;
#ifdef SYPHON
    ofxSyphonServer syphon;
#endif
#ifdef SPOUT
	ofxSpout::Sender spout_server;
#endif
    //ofxGuiGroup params;
    ofParameterGroup params;
    ofParameter<string> player_file;
    ofParameter<string> player_path;
    ofParameter<int> player_index;  //because pd only handle float
    string player_path_old;
    
    void setup(int index, int width, int height);
    void update();
    void load(string path);
    void draw(float x, float y, float w, float h);
    void allocate_fbo(int x, int y);
    void player_file_changed(string &s);
    void player_index_changed(int &i);
    int index_old;
    bool load_flag=0;
    string file_to_load;
    
    ofDirectory movie_dir;
    void set_movie_dir(ofDirectory& dir);
    void load_index(int index);

    ~svplay();
   
    int instance;
    ofFbo fbo_font;
    ofImage calibration_img;
    
};

#endif /* shared_video_playback_hpp */
