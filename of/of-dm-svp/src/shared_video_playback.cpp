//
//  shared_video_playback.cpp
//  vvideo_syphon
//
//  Created by gllm on 2021-11-06.
//

#include "shared_video_playback.hpp"



svplay::~ svplay()
{
    
}

void svplay::setup(int index, int width, int height)
{

    //setup 
    params.setName(to_string(index));
    instance=index;
    params.add(enable.set("enable",1));
    params.add(calibration.set("calibration",0));
    params.add(player_index.set("index", 0,0,10));
    player_index.addListener(this, &svplay::player_index_changed);
    params.add(player_file.set("file",""));
    player_file.addListener(this, &svplay::player_file_changed);
    
    
    allocate_fbo(width, height);
    player.play();
    player.setLoopState(OF_LOOP_NORMAL);
#ifdef SYPHON
    syphon.setName(to_string(index));
#endif
#ifdef SPOUT
	spout_server.init(to_string(index));
#endif
	//calibration_img.load("calibration/calibration.drawio.png");
	calibration_img.load("calibration/calibration.drawio.png");
    fbo_font.allocate(50, 50);
    fbo_font.begin();
    ofBackground(0, 0, 0, 0);
    ofSetColor(255, 255, 255);
    ofDrawBitmapString(index, 10, 10);
    fbo_font.end();
}

void svplay::update()
{
    fbo.begin();
    ofBackground(0,0,0,0);

    if(load_flag)
    {
        load(file_to_load);
        load_flag=0;
    }
    
    if(player.isLoaded()&& enable &&!calibration)
    {
        if(!player.isPlaying())
        {
            player.play();
        }
        //update movie
        player.update();
        // update texture to fbo
        float v_ratio=player.getHeight()/player.getWidth();
        player.draw(0, 0.5*(fbo.getWidth()-fbo.getWidth()*v_ratio), fbo.getWidth(), fbo.getWidth()*v_ratio);
    }
    
    if(calibration)
    {
        ofColor c;
        c.setHsb(((instance*33)%255), 255, 255);
        ofBackground(0,0,0);
        //ofSetColor(0, 0, 0);
        //ofDrawRectangle(2,2, fbo.getWidth()-4, fbo.getHeight()-4);
        ofSetColor(255, 255, 255);
        calibration_img.draw(0, 0, fbo.getWidth(), fbo.getHeight());
        fbo_font.draw(10, 10,fbo.getWidth()-20,fbo.getHeight()-20 );
        ofSetColor(c);
        ofNoFill();
        ofSetLineWidth(10); 
        ofDrawRectangle(0,0, fbo.getWidth(), fbo.getHeight());
 
        //ofDrawBitmapString(instance, 10, 10);
        //draw calib
    }
    
    fbo.end();

#ifdef SYPHON
    syphon.publishTexture(&fbo.getTexture());
#endif
#ifdef SPOUT
	spout_server.send(fbo.getTexture());
#endif
    
}

void svplay::draw(float x, float y, float w, float h)
{
    if(player.isLoaded()&&enable)
    {
        fbo.draw(x, y, w, h);
    }
}
void svplay::load(string path)
{
    player.load(path);
}

void svplay::allocate_fbo(int x, int y)
{
    fbo.clearDepthBuffer(0);
    fbo.allocate(x, y);
    
}

void svplay::player_file_changed(string &s)
{
    load_flag=1;
    file_to_load=s;
}

void svplay::player_index_changed(int &i)
{
    int index = int(i);
    if (index!=index_old)
    {
        load_index(index);
        index_old=index;
    }
}


void svplay::set_movie_dir(ofDirectory& dir)
{
    movie_dir=dir;
    player_index.setMax(movie_dir.size());
}
void svplay::load_index(int index)
{
	if (movie_dir.size())
	{
		player_file = movie_dir.getPath(index%movie_dir.size());

	}
}
