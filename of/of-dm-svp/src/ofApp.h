#pragma once

#include "ofMain.h"
#include "shared_video_playback.hpp"
#include "ofxOscParameterSync.h"
#include "ofxGui.h"
//#define SYPHON 1
#ifdef SYPHON
	#include "ofxSyphon.h"
#endif
#define SPOUT 1
#ifdef SPOUT
	#include "ofxSpout.h"
#endif

class ofApp : public ofBaseApp{

	public:
        #define SVP_SIZE 108
        svplay svplayer[SVP_SIZE];
    
    ofxOscParameterSync sync;
    ofxGuiGroup parameters;
    ofxGuiGroup params_system;
    ofParameter<int> num_player;
    void num_player_changed(int &num);
    ofxGuiGroup params_player;
    ofxPanel gui;
    
    ofParameter<bool> enable_all;

    void enable_all_changed(bool &b);
    ofParameter<bool> calibration_all;
    void calibration_all_changed(bool &b);
    
    ofParameter<int> matrix_column=12;
    ofParameter<int> matrix_row=9;
    ofParameter<int> matrix_offset= 3;
    ofParameter<int> matrix_w=85;
    ofParameter<int> fbo_w=1400;
    ofParameter<int> fbo_h=1050;
    
    void set_mov_folder();
    
    ofFbo fbo;
#ifdef SYPHON
    ofxSyphonServer syphon_server;
#endif    
#ifdef SPOUT
	ofxSpout::Sender spout_server;
#endif    

    
		void setup();
		void update();
		void draw();
    

		void keyPressed(int key);
		void keyReleased(int key);
		void mouseMoved(int x, int y );
		void mouseDragged(int x, int y, int button);
		void mousePressed(int x, int y, int button);
		void mouseReleased(int x, int y, int button);
		void mouseEntered(int x, int y);
		void mouseExited(int x, int y);
		void windowResized(int w, int h);
		void dragEvent(ofDragInfo dragInfo);
		void gotMessage(ofMessage msg);
		
};
