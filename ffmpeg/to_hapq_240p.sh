#!/bin/bash
INPUT_FOLDER="$1"
OUTPUT_FOLDER=hapq320p


mkdir -p "$INPUT_FOLDER"/"$OUTPUT_FOLDER"


#-c:a pcm_s16le
# 	ffmpeg   -i "$INPUT" -f lavfi -i anullsrc=channel_layout=stereo:sample_rate=44100 -vf scale=320:-2 -c:v hap -format hap_q -chunks 8 -shortest   "$INPUT".hapq320p.mov

process_ffmpeg()
{
	INPUT=$1
	INPUT_FILE=`basename $INPUT`
	INPUT_DIR=`dirname $INPUT`
	
	echo "transcoding $INPUT_FILE"
	ffmpeg   -i "$INPUT" -vf scale=320:-2 -c:v hap -format hap_q  -compressor none   "$INPUT_DIR"/"$OUTPUT_FOLDER"/"$INPUT_FILE".hapq320p.mov
}


for FILE in $INPUT_FOLDER/*; do
    [ -e "$FILE" ] || continue
    if ffprobe -v error -select_streams v:0 -show_entries stream=codec_type -of csv=p=0 "$FILE" | grep -q 'video'; then
      process_ffmpeg "$FILE" 
    fi
done


