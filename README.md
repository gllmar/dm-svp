# dm-svp

[Distributed Memories](https://gitlab.com/gllmar/distributed-memories)  shared video playback

## grid hap player
![grid_hap_player.png](docs/grid_hap_player.png)

## calibration
![grid_hap_player.png](docs/calibration.png)

## shared video texture

Main texture and dedicated outputs for each square

![main and shared via Syphon](./docs/syphon_a.png)

![calibraion via Syphon](./docs/syphon_c.png)


## OSC API 

* input port : 6667
* output port : 6666

All gui object are OSC enabled

### v0.1 
```
## $1-> instance number
/svplay/player/$1/index/ [int]  
/svplay/player/$1/enable/ [bool]  
```

### Dependencies

* https://openframeworks.cc/
* [ bangnoise/ofxHapPlayer](https://github.com/bangnoise/ofxHapPlayer)
* [astellato /ofxSyphon](https://github.com/astellato/ofxSyphon)
* [ elliotwoods/ofxSpout](https://github.com/elliotwoods/ofxSpout)
  
